var express = require('express');
var router = express.Router();
var request = require("request");
var dataRequest;

var requestOptions = {
  method: 'GET',
  uri: 'https://jsonplaceholder.typicode.com/users',
  json: true // Automatically stringifies the body to JSON
};

//Return all websites
router.get('/websites', (req, res) => {
  
  var requestPromise = httpRequest();
  var websiteList = [];

  requestPromise.then(function(result) {

    result.forEach(element => {
      websiteList.push(element['website']);
    });
    res.status(200).json(websiteList).end();

  }, function(err) {
    res.status(422).json(err).end();
  });

});

//Returns all users (name, email and company) in alphabetical order
router.get('/', (req, res) => {

  var requestPromise = httpRequest();
  var userList = [];

  requestPromise.then(function(result) {

    result.forEach(element => {
      var user = {
        name: element['name'],
        email: element['email'],
        company: element['company']['name']
      };

      userList.push(user);
    });

    userList = alphabeticalOrder(userList);

    res.status(200).json(userList).end();

  }, function(err) {
    res.status(422).json(err).end();
  });

});

//Returns all users with suites at address
router.get('/find_address_suite', (req, res) => {
  
  var requestPromise = httpRequest();
  var userList = [];

  requestPromise.then(function(result) {

    result.forEach(element => {
      if(element['address']['suite'].toLowerCase().includes("suite")){
        userList.push(element);
      }
    });

    res.status(200).json(userList).end();

  }, function(err) {
    res.status(422).json(err).end();
  });

});

//Method created to send the http request
function httpRequest() {
  
  return new Promise(function(resolve, reject) {
    
    //If data already exists does not send a new request.
    if(dataRequest){
      resolve(dataRequest);
    } else {
      request.get(requestOptions, function(err, resp, body) {
        if (err) {
          reject(err);
        } else {
          dataRequest = body;
          resolve(dataRequest);
        }
      });
    };
  });

};

//Order by name, then by email and finally by company
function alphabeticalOrder(array) {

  return array.sort(function (a, b) {
    if (a.name > b.name) {
      return 1;
    }
    if (a.name < b.name) {
      return -1;
    }
    if (a.email > b.email) {
      return 1;
    }
    if (a.email < b.email) {
      return -1;
    }
    if (a.company > b.company) {
      return 1;
    }
    if (a.company < b.company) {
      return -1;
    }
    
    // a must be equal to b
    return 0;
  });
};

module.exports = router;
